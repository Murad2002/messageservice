package com.example.messagesender.service;


import com.example.messagesender.dto.SlackDto;
import com.example.messagesender.feign.SlackClient;
import com.example.messagesender.model.SlackMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageService {
    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private final SlackClient client;
    private static final String urlSlackWebHook = "https://hooks.slack.com/services/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";

    //    First create workspace (if not exists) then create slack app and webhook.
    public void sendMessageToSlackOld(String message) {
        String messageBuider = "My message : " + message;
        Payload payload = Payload.builder()
                .channel("#test-project")
                .username("Bob Bot")
                .iconEmoji(":rocket:")
                .text(message)
                .build();
        try {
            WebhookResponse webhookResponse = Slack.getInstance().send(
                    urlSlackWebHook, payload);
            log.info("code -> " + webhookResponse.getCode());
            log.info("body -> " + webhookResponse.getBody());
        } catch (IOException e) {
            log.error("Unexpected Error! WebHook:" + urlSlackWebHook);
        }
    }

    public void sendMessageToSlackNew(SlackMessage message) {

        HttpPost httpPost = new HttpPost(urlSlackWebHook);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(message);

            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpClient.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessageToSlackFeign(SlackDto message) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(message.getTestDto());
        var request = SlackMessage.builder()
                .channel(message.getChannel())
                .username(message.getUsername())
                .icon_emoji(message.getIcon_emoji())
                .text(json)
                .build();
        client.sendMessageToSlack(request);
    }

    // https://dds861.medium.com/sending-message-to-telegram-group-channel-using-bot-from-android-or-java-apps-3c68ffe04a46
// https://api.telegram.org/bot<YourBOTToken>/getUpdates
    public void sendMessageToTelegramBot(String text) {
        try {
            String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
//            urlString = String.format(urlString, API_TOKEN, CHAT_ID, text);

            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            is.close();
        } catch (IOException ex) {
            ex.printStackTrace();

        }
    }
}
