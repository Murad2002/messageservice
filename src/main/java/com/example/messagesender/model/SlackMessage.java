package com.example.messagesender.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@AllArgsConstructor
@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SlackMessage implements Serializable {

    String channel;
    String username;
    String text;
    String icon_emoji;
}

