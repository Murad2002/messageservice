package com.example.messagesender.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SlackDto {

    String channel;
    String username;
    TestDto testDto;
    String icon_emoji;
}
