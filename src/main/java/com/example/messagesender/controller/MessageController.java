package com.example.messagesender.controller;

import com.example.messagesender.dto.SlackDto;
import com.example.messagesender.model.SlackMessage;
import com.example.messagesender.service.MessageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/send-message")
@RequiredArgsConstructor
public class MessageController {
    private final MessageService messageService;

    @PostMapping("/slack-old/{message}")
    public ResponseEntity<String> sendSimpleMessageToSlackOld(
            @PathVariable String message) {
        messageService.sendMessageToSlackOld(message);
        return ResponseEntity.ok(message);
    }

    @PostMapping("/slack-new")
    public ResponseEntity<SlackDto> sendSimpleMessageToSlackNew(
            @RequestBody SlackDto message) throws JsonProcessingException {
        messageService.sendMessageToSlackFeign(message);
        return ResponseEntity.ok(message);
    }

    @PostMapping("/telegram/{message}")
    public ResponseEntity<String> sendMessageToTelegram(
            @PathVariable String message) {
        messageService.sendMessageToTelegramBot(message);
        return ResponseEntity.ok(message);
    }

}
