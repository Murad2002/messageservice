package com.example.messagesender.feign;

import com.example.messagesender.model.SlackMessage;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "slack-client", url = "${slack-feign.host}")
public interface SlackClient {

    @PostMapping
    @Headers("Content-Type: application/json")
    void sendMessageToSlack(SlackMessage dto);
}
